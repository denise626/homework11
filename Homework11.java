
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Homework11 {

    public static void main(String[] args) {

        List<Integer> arrayListOfInt1 = new ArrayList<Integer>();
        arrayListOfInt1.add(-2);
        arrayListOfInt1.add(-3);
        arrayListOfInt1.add(6);
        arrayListOfInt1.add(3);
        System.out.println(arrayListOfInt1);

        List<Integer> arrayListOfInt2 = new ArrayList<Integer>();
        arrayListOfInt2.add(-2);
        arrayListOfInt2.add(-4);
        arrayListOfInt2.add(5);
        arrayListOfInt2.add(-3);
        System.out.println(arrayListOfInt2);

        arrayListOfInt1.addAll(arrayListOfInt2);
        Collections.sort(arrayListOfInt1);
        System.out.println(arrayListOfInt1);
        System.out.println("Minimum value of array is " + (arrayListOfInt1.get(0)));
    }
}
